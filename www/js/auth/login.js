(function () {
  'use strict';
  var controllerId = 'loginCtrl';

  angular.module('bakimliApp.auth')
    .controller(controllerId,
      ['$state', 'authService', 'sessionService', 'UIHelper', '$window', 'ConfigServer', '$cordovaOauth', '$log', login]);

  function login($state, authService, sessionService, UIHelper, $window, ConfigServer, $cordovaOauth, $log) {

    var vm = this;

  	vm.loginData = {};
    vm.user = vm.loginData;

    vm.doLogin = function() {
      console.log('Doing login', vm.loginData);

      if ( Object.keys(vm.loginData).length <= 0 ||
        vm.loginData.username === null || vm.loginData.username === undefined ||
        vm.loginData.password === null || vm.loginData.password === undefined ) {

        $ionicPopup.alert({
            title: "You must entered username and password."
        });

        return;
      }

      authService.login(vm.loginData).then(function(response) {
        console.log("Login successed!", response);

        $window.sessionStorage.token = response.token;

        // clear login form
        vm.loginData = { username: "", password: ""};

        // goto home
        $state.go('app.businesses');
      },
      function(error) {
        // Erase the token if the user fails to log in
        delete $window.sessionStorage.token;

        if (error !== null && error.email) {
          UIHelper.showAlert(error.email);
        }
        else {
          // Handle login errors here
          UIHelper.showAlert('Error: Invalid user or password.');
        }
      });
    };

    vm.doFbLogin = function() {
      $cordovaOauth.facebook(ConfigServer.fbAppId, ["email"])
      .then(function(result) {
        if (result.access_token) {
          window.localStorage['accessToken'] = result.access_token;
          return result.access_token;
        }
      }, function(error) {
        UIHelper.showAlert(error);
      })
      .then(function(result){
        if (angular.isDefined(result)) {
          authService.loginFacebook(result)
          .then(function(success){
            $log.debug('Login successed!');
            $window.sessionStorage.token = success.key;

            // get user profile
            authService.getProfile().then(function(responseData) {
              sessionService.setUserInfo(responseData);
            });

            $state.go('app.businesses');
          },function(errors) {
            // Erase the token if the user fails to log in
            delete $window.sessionStorage.token;

            // Handle login errors here
            UIHelper.showAlert('Error: Invalid user or password.');
          });
        }
      });
    };
  }
})();
