(function () {
  'use strict';
  var appAuth = angular.module('bakimliApp.auth', []);

  appAuth.config([
    '$stateProvider',
    '$urlRouterProvider',
    function(
    $stateProvider,
    $urlRouterProvider
  ){
    $stateProvider
    .state('login', {
  		url: "/login",
  		templateUrl: 'js/auth/login.html',
  		controller: 'loginCtrl as vm'
  	})
  }]);
})();
