(function () {
  'use strict';

  var app = angular.module('bakimliApp');

  app.config(['$stateProvider', '$urlRouterProvider', '$ionicConfigProvider', function($stateProvider, $urlRouterProvider, $ionicConfigProvider) {

    $ionicConfigProvider.backButton.previousTitleText(false).text('');

    // main state
    $stateProvider
    .state('app', {
      url: '/app',
      abstract: true,
      templateUrl: 'js/layout/menu.html',
      controller: 'menu as vm'
    });


    //Default Route for the whole app
    $urlRouterProvider.otherwise('/login');
  }]);

  app.config(['$translateProvider', function($translateProvider) {
    ///////////////////////////////////////////////////
    /// translate
    $translateProvider.useStaticFilesLoader({
        prefix: 'i18n/',
        suffix: '.json'
    });
    $translateProvider
      .registerAvailableLanguageKeys(['en', 'tr'], {
          'tr_*': 'tr',
          '*': 'en'
      });

    var lang = window.localStorage['language'];
    if (lang)
        $translateProvider.preferredLanguage(lang);
    else
        $translateProvider.determinePreferredLanguage();

    $translateProvider.fallbackLanguage("en");
  }]);

  var configServer = {
    version: 'v1',
    url: 'http://bakim.li:80/api/',
    fbAppId: '1721377898085852'
  };

  var defaultTimeZone = 'America/Los_Angeles';
  var defaultDatetimeFormat = 'MMM D, YYYY hh:mm A z';

  app.constant('ConfigServer', configServer);
  app.constant('DefaultTimezone', defaultTimeZone);
  app.constant('DefaultDatetimeFormat', defaultDatetimeFormat);

})();
