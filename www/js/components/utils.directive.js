(function () {
  'use strict';

	var app = angular.module('bakimliApp');

  app.directive('actualSrc', function () {

    var directive = {
      link: postLink
    };

    return directive;

    function postLink(scope, element, attrs) {
      attrs.$observe('actualSrc', function(newVal, oldVal){
         if(newVal !== undefined){
           var img = new Image();
           img.src = attrs.actualSrc;
           angular.element(img).bind('load', function () {
              element.attr("src", attrs.actualSrc);
           });
         }
      });
    }
  });
  
  app.directive('stringToNumber', function() {

    var directive = {
      require: 'ngModel',
      link: link
    };

    return directive;

    function link(scope, element, attrs, ngModel) {
      ngModel.$parsers.push(function(value) {
        return '' + value;
      });
      ngModel.$formatters.push(function(value) {
        return parseFloat(value, 10);
      });
    }
  });
  app.directive('validNumber', function() {

    var directive = {
      require: '?ngModel',
      link: link
    }

    return directive;

    function link(scope, element, attrs, ngModelCtrl) {
      if(!ngModelCtrl) {
        return;
      }

      ngModelCtrl.$parsers.push(function(val) {
        if (angular.isUndefined(val)) {
            var val = '';
        }
        var clean = val.replace( /[^0-9]+/g, '');
        if (val !== clean) {
          ngModelCtrl.$setViewValue(clean);
          ngModelCtrl.$render();
        }
        return clean;
      });

      element.bind('keypress', function(event) {
        if(event.keyCode === 32) {
          event.preventDefault();
        }
      });
    }
  });

  app.directive('clickForOptions', ['$ionicGesture',
    function($ionicGesture) {

    var directive = {
      restrict: 'A',
      link: link
    }

    return directive;

    function link(scope, element, attrs) {
      $ionicGesture.on('tap', function(e){

        // Grab the content
        var content = element[0].querySelector('.item-content');

        // Grab the buttons and their width
        var buttons = element[0].querySelector('.item-options');

        if (!buttons) {
            console.log('There are no option buttons');
            return;
        }
        var buttonsWidth = buttons.offsetWidth;

        ionic.requestAnimationFrame(function() {
          content.style[ionic.CSS.TRANSITION] = 'all ease-out .25s';

          if (!buttons.classList.contains('invisible')) {
            console.log('close');
            content.style[ionic.CSS.TRANSFORM] = '';
            setTimeout(function() {
                buttons.classList.add('invisible');
            }, 250);
          } else {
            buttons.classList.remove('invisible');
            content.style[ionic.CSS.TRANSFORM] = 'translate3d(-' + buttonsWidth + 'px, 0, 0)';
          }
        });

      }, element);
    }
  }]);
})();
