(function () {
  'use strict';

	var app = angular.module('bakimliApp');

  app.filter('filterSearchByName', function() {
    return function(input, search) {
      if (!input) return input;
      if (!search) return input;
      var expected = ('' + search).toLowerCase();
      var result = {};
      angular.forEach(input, function(value, key) {
        var actual = (value.name).toLowerCase();
        if (actual.indexOf(expected) !== -1) {
          result[key] = value;
        }
      });
      return result;
    }
  });

  app.filter('bakimliCurrency', ['$filter', function ($filter) {
    return function(input) {
      input = parseFloat(input);

      if(input % 1 === 0) {
        input = input.toFixed(0);
      }
      else {
        input = input.toFixed(2);
      }

      return input.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",") + ' TL';
    };
  }]);

  app.filter('ifEmpty', function() {
    return function(input, defaultValue) {
      if (angular.isUndefined(input) || input === null || input === '') {
        return defaultValue;
      }

      return input;
    }
  });
})();
