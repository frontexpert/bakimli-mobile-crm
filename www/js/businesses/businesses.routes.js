(function () {
  'use strict';
  
  var app = angular.module('bakimliApp.businesses', []);

  app.config([
    '$stateProvider',
    '$urlRouterProvider',
    function(
    $stateProvider,
    $urlRouterProvider
  ){

    $stateProvider
    .state('app.businesses', {
  		url: '/businesses',
  		views: {
  			'menuContent': {
  				templateUrl: 'js/businesses/businesses.html',
  				controller: 'businesses as vm'
  			}
  		}
  	})
  }]);
})();
