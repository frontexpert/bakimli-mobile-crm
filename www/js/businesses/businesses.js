(function () {
  'use strict';
	
  var controllerId = 'businesses';
	var app = angular.module('bakimliApp.businesses')
		.controller(controllerId,
			['$rootScope', '$state', 'authService', 'professionalsService',	'$ionicPopup', 'sessionService', '$ionicHistory', businesses]);

	function businesses($rootScope, $state, authService,professionalsService,	$ionicPopup, sessionService, $ionicHistory) {

		var vm = this;

		vm.professionals = {};
		vm.clients = {};
		vm.pPk = "";

		// init controller
		vm.init = function() {
			// show logout button in vm view
			$rootScope.hideLogout = false;

			// Get user prfile from logged in
			var userInfo = sessionService.getUserInfo();
			if ( userInfo !== undefined && userInfo.professional !== null ) {
				vm.pPk = userInfo.professional.pk;
			}
			else {
				vm.message = "You don't have a professional profile.";
				$rootScope.canDrag = false;
				return;
			}

			// get professional info for menu
			professionalsService.getProfessionalByPk(vm.pPk).then(function(success) {
				console.log("fetched professional for user logged in");
				$rootScope.user = success;
			}, function(error) {
				if ( angular.isDefined(error.details) ) {
					$ionicPopup.alert({
	          title: error.details
	        });
				}
				else {
					// Handle login errors here
	        $ionicPopup.alert({
	          title: 'Error: Lost internet connection. Please check your internet connection.'
	        });
				}
			});
		};

		vm.init();

		vm.assignBusiness = function() {
			$ionicHistory.nextViewOptions({
			  disableAnimate: false,
			  disableBack: true
			});
			$state.go('app.receipts');
		}
	}
})();
