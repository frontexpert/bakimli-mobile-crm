(function () {
  'use strict';

  var app = angular.module('bakimliApp', [
    // Ionic modules
    'ionic',                // ionic main module
    'ion-datetime-picker',  // datetime picker
    'ionic-toast',          // toast

    // Angular Modules
    'ngCordova',      // Cordova
    'ngMessages',     // Messages
    'ngCordovaOauth', // cordova oauth
    'ngResource',     // resource

    // Custom modules
    'bakimliApp.auth',
    'bakimliApp.businesses',
    'bakimliApp.receipts',

    // 3rd Party Modules
    'pascalprecht.translate'
  ]);

  app.run(['$ionicPlatform', '$rootScope', function($ionicPlatform, $rootScope) {
    $ionicPlatform.ready(function() {
      // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
      // for form inputs)
      if (window.cordova && window.cordova.plugins && window.cordova.plugins.Keyboard) {
        cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
        cordova.plugins.Keyboard.disableScroll(true);

      }
      if (window.StatusBar) {
        // org.apache.cordova.statusbar required
        StatusBar.styleDefault();
      }
    });

    // Set default nav button status
    $rootScope.canDrag = true; // menu button
    $rootScope.hideLogout = true; // logout button
  }]);

})();
