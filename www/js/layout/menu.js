(function () {
  'use strict';

	var controllerId = 'menu';
	angular.module('bakimliApp')
		.controller(controllerId,
			['$rootScope', '$state', '$timeout', 'sessionService', '$window', menu]);

	function menu($rootScope, $state, $timeout, sessionService, $window){

		var vm = this;

		$rootScope.cls = "bakimli-menu";

		this.logout = function() {
			// Erase the token if the user fails to log in
	    delete $window.sessionStorage.token;

			sessionService.destroy();

	    $timeout(function() {
	      $state.go('login');
	    }, 1000);
		}
	}
})();
