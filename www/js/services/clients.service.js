(function () {
  'use strict';

  var serviceId = 'clientsService';

	angular.module('bakimliApp')
		.factory(serviceId, ['$rootScope', '$http',	'$q',	'Utils', 'ConfigServer', '$window', clientsService]);

	function clientsService($rootScope,	$http, $q, Utils,	ConfigServer,	$window) {

		var services = {
			getClients: getClients,
			getClientByPk: getClientByPk
		};

		return services;

		function getClients() {

			var def = $q.defer();
			Utils.showLoading();

			$http.get(ConfigServer.url + ConfigServer.version + '/clients/',
				{headers: {
					'Authorization': 'Token ' + $window.sessionStorage.token
				}})
	    .success(function(response) {
				Utils.hideLoading();
				def.resolve(response);
			})
			.error(function(err) {
				Utils.hideLoading();
				def.reject(err);
			});

			return def.promise;
		}

		function getClientByPk(pk) {
			var def = $q.defer();
			Utils.showLoading();

			$http.get(ConfigServer.url + ConfigServer.version + '/clients/' + pk + '/',
				{headers: {
					'Authorization': 'Token ' + $window.sessionStorage.token
				}})
	    .success(function(response) {
				Utils.hideLoading();
				def.resolve(response);
			})
			.error(function(err) {
				Utils.hideLoading();
				def.reject(err);
			});

			return def.promise;
		}
	}
})();
