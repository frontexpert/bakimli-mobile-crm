(function () {
  'use strict';

  var serviceId = 'authService';

	angular.module('bakimliApp')
		.factory(serviceId,
			['$http', '$q',	'ConfigServer',	'sessionService',	'Utils', authService]);

	function authService($http,	$q,	ConfigServer,	sessionService,	Utils) {

		var authService = {
      login: login,
      loginFacebook: loginFacebook,
      getProfile: getProfile
    };

		return authService;

		function login(data) {
			var def = $q.defer();
			Utils.showLoading();

			var requestData = {email:data.username, password: data.password};

			$http.post(ConfigServer.url + ConfigServer.version + '/auth/login/', requestData)
	    .success(function(response) {
				Utils.hideLoading();
				if ( response.professional !== null )
					sessionService.setUserInfo(response);
				def.resolve(response);
			})
			.error(function(err) {
				Utils.hideLoading();
				def.reject(err);
			});

			return def.promise;
		};

		function loginFacebook(accessToken) {
			var def = $q.defer();
			Utils.showLoading();

			$http({
				url: ConfigServer.url + ConfigServer.version + '/auth/login/facebook/',
				method: 'POST',
				data: {access_token: accessToken}
			})
			.success(function(response) {
				Utils.hideLoading();
				def.resolve(response);
			})
			.error(function(err) {
				Utils.hideLoading();
				def.reject(err);
			});

			return def.promise;
		};

		function getProfile() {
			var def = $q.defer();
			Utils.showLoading();

			$http.get(ConfigServer.url + ConfigServer.version + '/auth/profile/')
	    .success(function(response) {
				Utils.hideLoading();
				def.resolve(response);
			})
			.error(function(err) {
				Utils.hideLoading();
				def.reject(err);
			});

			return def.promise;
		};

	}
})();
