(function () {
  'use strict';

  var serviceId = 'Utils';

	angular.module('bakimliApp')
    .factory(serviceId,
      ['DefaultTimezone', 'DefaultDatetimeFormat', '$ionicHistory', '$ionicLoading', Utils]);

  function Utils(DefaultTimezone, DefaultDatetimeFormat, $ionicHistory, $ionicLoading) {
    var UtilsSrv = {
      debug: false,
      config: null,
      formatDate: formatDate,
      clearCachedPreviousView: clearCachedPreviousView,
      clearCachedView: clearCachedView,
      showLoading: showLoading,
      hideLoading: hideLoading,
      goBack: goBack,
      getDomainName: getDomainName,
      getCurrentStateName: getCurrentStateName,
      isValidPhone: isValidPhone,
      getActualPhoneNumber: getActualPhoneNumber,
      isPicture:isPicture,
      convertObject: convertObject,
      convertObjects: convertObjects,
      parseString: parseString,
      isUndefinedOrNull: isUndefinedOrNull
    };

    return UtilsSrv;

    function formatDate(dt, format) {
      if (!format) {
        format = DefaultDatetimeFormat;
      }
      return moment(dt).tz(DefaultTimezone).format(format);
    }

    function clearCachedPreviousView() {
      var backView = $ionicHistory.backView();
      return $ionicHistory.clearCache([backView ? backView.stateId:'']);
    }

    function clearCachedView(stateIds) {// stateIds should be array
      return $ionicHistory.clearCache(stateIds);
    }

    function showLoading() {
      $ionicLoading.show({
        template: '<i class="ion-loading-c"></i> Loading...'
      });
    }

    function hideLoading() {
      $ionicLoading.hide();
    }

    function goBack() {
      $ionicHistory.goBack();
    }

    function getDomainName(url) {
      var domain;
      if (url.indexOf("://") > -1) {
        domain = url.split('/')[2];
      }
      else {
        domain = url.split('/')[0];
      }
      domain = domain.split(':')[0];

      return domain;
    }
    function getCurrentStateName() {
      return $ionicHistory.currentStateName();
    }

    function isValidPhone(telnum) {
      telnum = this.getActualPhoneNumber(telnum);
      var valid = false;
      if ((telnum.indexOf('+') === -1 && telnum.length === 10) || (telnum.indexOf('+') === 0 && telnum.length >= 12)) {
        valid = true;
      }
      return valid;
    }
    function getActualPhoneNumber(telnum) {
      return String(telnum).replace(/[\(\)\s-_]/g, '');
    }

    function isPicture(item) {
      return item.contentType && item.contentType.indexOf('image/') === 0;
    }

    function convertObject(object) {
      if(_.has(object, 'className')){
        try{
          return _.extend({id: object.id, objectId: object.id }, UtilsSrv.convertObject(object.attributes));
        }catch(error){
          return {};
        }
      }else{
        try{
          return angular.fromJson(angular.toJson(object));
        }catch(error){
          return {};
        }
      }
    }

    function convertObjects(objects) {
      return _.map(objects, function (item) {
        return  UtilsSrv.convertObject(item);
      });
    }

    function parseString(object) {
      if(object.__type === 'Date'){
        return new Date(object.iso);
      }else{
        return object || '';
      }
    }

    function isUndefinedOrNull(obj) {
      return !angular.isDefined(obj) || obj===null;
    }
  }
})();
