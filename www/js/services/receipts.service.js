(function () {
  'use strict';

  var serviceId = 'receiptsServices';

	angular.module('bakimliApp')
		.factory(serviceId,
			['$rootScope', '$http', '$q',	'Utils', 'ConfigServer', '$window', receiptsServices]);

	function receiptsServices($rootScope,	$http, $q, Utils,	ConfigServer,	$window) {

		var services = {
			getReceipts: getReceipts,
			getReceiptByPk: getReceiptByPk,
			createReceipt: createReceipt
		}

		return services;

		function getReceipts(options) {

			var def = $q.defer();
			Utils.showLoading();

			var url = ConfigServer.url + ConfigServer.version + '/receipts/?page=' + options.page;

			$http({
				url: url,
				method: "GET",
				// data: JSON.stringify(options),
				headers: {
					'Authorization': 'Token ' + $window.sessionStorage.token
				}
			})
	    .success(function(response) {
				Utils.hideLoading();
				def.resolve(response);
			})
			.error(function(err) {
				Utils.hideLoading();
				def.reject(err);
			});

			return def.promise;
		}

		function getReceiptByPk(pk) {
			var def = $q.defer();
			Utils.showLoading();

			$http.get(ConfigServer.url + ConfigServer.version + '/receipts/' + pk + '/',
				{headers: {
					'Authorization': 'Token ' + $window.sessionStorage.token
				}})
	    .success(function(response) {
				Utils.hideLoading();
				def.resolve(response);
			})
			.error(function(err) {
				Utils.hideLoading();
				def.reject(err);
			});

			return def.promise;
		}
		
		function createReceipt(cart) {
			var def = $q.defer();
			Utils.showLoading();

			$http({
				url: ConfigServer.url + ConfigServer.version + '/receipts/',
				method: 'POST',
				data: cart,
				headers: {
					'Authorization': 'Token ' + $window.sessionStorage.token
				}
			})
	    .success(function(response) {
				Utils.hideLoading();
				def.resolve(response);
			})
			.error(function(err) {
				Utils.hideLoading();
				def.reject(err);
			});

			return def.promise;
		}
	}
})();
// its not able to use yet
// .factory('receiptsResource', ['$resource', 'ConfigServer', '$window', function($resource, ConfigServer, $window) {
// 	var transformRequest = function(data, headersGetter){
//      var headers = headersGetter();
//      headers['Authorization'] = 'Token ' + $window.sessionStorage.token;
//   };
//
//   var headers = {
//   	'Authorization': 'Token ' + $window.sessionStorage.token
//   };
//
//   return $resource(
//     ConfigServer.url + ConfigServer.version + '/receipts/',
//     {},
//     {
//       'list': {
//           method: 'GET',
//           isArray: false,
//           headers: headers
//       },
//     }
//   );
// }]);
