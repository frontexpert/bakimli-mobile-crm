(function () {
  'use strict';

  var serviceId = 'professionalsService';

	angular.module('bakimliApp')
		.factory(serviceId,
			['$http', '$q',	'Utils', 'ConfigServer', '$window', professionalsService]);

	function professionalsService($http, $q, Utils,	ConfigServer,	$window) {

		var services = {
			getProfessionalByPk: getProfessionalByPk,
			getProfessionalClientsByPk: getProfessionalClientsByPk,
			getServicesByPk: getServicesByPk,
			getProductsByPk: getProductsByPk,
			createProfessionalClient: createProfessionalClient
		};

		return services;

		function getProfessionalByPk(pk) {
			var def = $q.defer();
			Utils.showLoading();

			$http.get(ConfigServer.url + ConfigServer.version + '/professionals/' + pk + '/',
				{headers: {
					'Authorization': 'Token ' + $window.sessionStorage.token
				}})
	    .success(function(response) {
				Utils.hideLoading();
				def.resolve(response);
			})
			.error(function(err) {
				Utils.hideLoading();
				def.reject(err);
			});

			return def.promise;
		}

		function getProfessionalClientsByPk(pk) {
			var def = $q.defer();
			Utils.showLoading();

			$http.get(ConfigServer.url + ConfigServer.version + '/professionals/' + pk + '/clients/?page_size=all',
				{headers: {
					'Authorization': 'Token ' + $window.sessionStorage.token
				}})
	    .success(function(response) {
				Utils.hideLoading();
				def.resolve(response);
			})
			.error(function(err) {
				Utils.hideLoading();
				def.reject(err);
			});

			return def.promise;
		}

		function getServicesByPk(pk) {
			var def = $q.defer();
			Utils.showLoading();

			$http.get(ConfigServer.url + ConfigServer.version + '/professionals/' + pk + '/services/',
				{headers: {
					'Authorization': 'Token ' + $window.sessionStorage.token
				}})
	    .success(function(response) {
				Utils.hideLoading();
				def.resolve(response);
			})
			.error(function(err) {
				Utils.hideLoading();
				def.reject(err);
			});

			return def.promise;
		}

		function getProductsByPk(pk) {
			var def = $q.defer();
			Utils.showLoading();

			$http.get(ConfigServer.url + ConfigServer.version + '/professionals/' + pk + '/products/',
				{headers: {
					'Authorization': 'Token ' + $window.sessionStorage.token
				}})
	    .success(function(response) {
				Utils.hideLoading();
				def.resolve(response);
			})
			.error(function(err) {
				Utils.hideLoading();
				def.reject(err);
			});

			return def.promise;
		}

		function createProfessionalClient(proId, clientData) {
			var def = $q.defer();
			Utils.showLoading();

			$http({
				url: ConfigServer.url + ConfigServer.version + '/professionals/' + proId + '/clients/',
				method: "POST",
				data: clientData,
				headers: {
					'Authorization': 'Token ' + $window.sessionStorage.token
				}
			})
	    .success(function(response) {
				Utils.hideLoading();
				def.resolve(response);
			})
			.error(function(err) {
				Utils.hideLoading();
				def.reject(err);
			});

			return def.promise;
		}
	}
})();
// .factory('professionalsResource', ['$resource', 'ConfigServer', function($resource, ConfigServer) {
//
//   return $resource(
//     ConfigServer.url + ConfigServer.version + '/professionals/',
//     {},
//     {
//         'list': {
//             method: 'GET',
//             isArray: false
//         }
//     }
//   );
// }]);
