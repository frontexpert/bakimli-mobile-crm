(function () {
  'use strict';

  var serviceId = 'businessesService';

	angular.module('bakimliApp')
		.factory(serviceId,
			['$http', '$q',	'ConfigServer', 'Utils', businessesService]);

	function businessesService($http,	$q,	ConfigServer, Utils) {

		var service = {
			getProfessionals: getProfessionals,
			getSalons: getSalons
		};

		return service;

		function getProfessionals() {
			var def = $q.defer();
			Utils.showLoading();

			$http.get(ConfigServer.url + ConfigServer.version + '/professionals/')
	    .success(function(response) {
				Utils.hideLoading();
				def.resolve(response);
			})
			.error(function(err) {
				Utils.hideLoading();
				def.reject(err);
			});

			return def.promise;
		}

		function getSalons() {
			var def = $q.defer();
			Utils.showLoading();

			$http.get(ConfigServer.url + ConfigServer.version + '/salons/')
	    .success(function(response) {
				Utils.hideLoading();
				def.resolve(response);
			})
			.error(function(err) {
				Utils.hideLoading();
				def.reject(err);
			});

			return def.promise;
		}

	}
})();
