(function () {
  'use strict';

  var serviceId = 'cartProperties';

	angular.module('bakimliApp')
		.factory(serviceId,
			[cartProperties]);

	function cartProperties(){
		var cart = {};
		cart.proId = '';
		cart.client = null;
		cart.appointmentTime = new Date();
		cart.services = [];
		cart.products = [];
		cart.total = 0;
		cart.updateTotal = function() {
	    var totalPrice = 0;
	    // get sum of price for services
	    for (var i = cart.services.length - 1; i >= 0; i--) {
	      totalPrice += parseInt(cart.services[i].price);
	    }
	    // get sum of price for products
	    for (var i = cart.products.length - 1; i >= 0; i--) {
	      totalPrice += parseInt(cart.products[i].price);
	    };

		  cart.total = totalPrice;
		};

		cart.reset = function() {
			cart.proId = '';
			cart.client = null;
			cart.appointmentTime = new Date();
			cart.services = [];
			cart.products = [];
			cart.total = 0;
		};

		return cart;
	}
})();
