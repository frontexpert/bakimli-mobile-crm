(function () {
  'use strict';

  var serviceId = 'sessionService';

	angular.module('bakimliApp')
    .factory(serviceId, ['$window', sessionService]);

  function sessionService($window) {

    var sessionService = {
      getUserInfo: getUserInfo,
      setUserInfo: setUserInfo,
      destroy: destroy
    };

    return sessionService;

    function getUserInfo() {
      return $window.sessionStorage["userInfo"] !== undefined ? JSON.parse($window.sessionStorage["userInfo"]) : undefined;
    }

    function setUserInfo(userInfo) {
      $window.sessionStorage["userInfo"] = JSON.stringify(userInfo);
    }

    function destroy() {
      delete $window.sessionStorage["userInfo"]
    }
  }
})();
