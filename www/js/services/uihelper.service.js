(function () {
  'use strict';

  var serviceId = 'UIHelper';

	angular.module('bakimliApp')
		.factory('UIHelper',
			['$rootScope', 'Utils',	'$ionicPopup', '$ionicLoading', '$timeout', '$translate', 'ionicToast', UIHelper]);

	function UIHelper($rootScope,	Utils, $ionicPopup, $ionicLoading,	$timeout,	$translate,	ionicToast){

		var services = {
			showAlert: showAlert,
			showToast: showToast,
			promptForPassword: promptForPassword,
			confirmAndRun: confirmAndRun,
			blockScreen: blockScreen,
			shareText: shareText,
			translate: translate,
			getCurrentLanguage: getCurrentLanguage,
			changeLanguage: changeLanguage
		};

		return services;

    function showAlert(captionRes, plainSuffix) {
        console.log(captionRes);
        $translate(captionRes).then(function (caption) {
            if (plainSuffix)
                  caption+=plainSuffix;
              Utils.hideLoading();
              $ionicPopup.alert({
                  title: caption
              })
          });
		}

		function showToast(message, position, stick, time) {
			$translate(message).then(function (caption) {
        Utils.hideLoading();
				ionicToast.show(caption, position, stick, time);
      });
		}

    function promptForPassword (onOk, freeChoice) {
        var titleText = freeChoice ? 'services.uiHelper.password.title.choose' : 'services.uiHelper.password.title.enter';
        var placeholderText = freeChoice ? 'services.uiHelper.password.placeholder.choose' : 'services.uiHelper.password.placeholder.enter';
        this.translate([titleText, placeholderText, 'general.btn.ok', 'general.btn.cancel']).then(function (t) {
            $ionicPopup.prompt({
                title: t[0],
                inputType: 'password',
                inputPlaceholder: t[1],
                okText: t[2],
                cancelText: t[3]
            }).then(function (res) {
                if (res || res == '') {
                    onOk(res)
                }
            });
        });
		}
		
		function confirmAndRun(captionRes, textRes, onConfirm) {
		    this.translate([captionRes, textRes, 'general.btn.ok', 'general.btn.cancel']).then(function (t) {
		        $ionicLoading.hide();
		        var popup = $ionicPopup.confirm({
		            title: t[0],
		            template: t[1],
		            okText: t[2],
		            cancelText: t[3]
		        });
		        popup.then(function (res) {
		            if (res) {
		                onConfirm();
		            }
		        });
		    });
		}

		function blockScreen(textRes, timeoutSec) {
		    $translate(textRes).then(function (text) {
		        $ionicLoading.show({
		            template: text
		        });
		        $timeout(function () {
		            $ionicLoading.hide();
		        }, timeoutSec * 1000);
		    });
		}

		function shareText(captionRes, textRes){
		    $translate([captionRes, textRes]).then(function (translations) {
		        var caption = translations[captionRes];
		        var text = translations[textRes];
		        if (window.plugins) {
		            window.plugins.socialsharing.share(text, caption);
		        }
		        else {
		            var subject = caption.replace(' ', '%20').replace('\n', '%0A');
		            var body = text.replace(' ', '%20').replace('\n', '%0A');
		            window.location.href = 'mailto:?subject=' + subject + '&body=' + body;
		        }
		    });
		}

		function translate(keys){
		    var promise = new Promise(function (resolve, reject) {
		        $translate(keys).then(function (translations) {
		            var t = [];
		            for (i = 0; i < keys.length; i++) {
		                var key = keys[i];
		                t.push(translations[key]);
		            }
		            resolve(t);
		        })
		        .catch(reject);
		    });
		    return promise;
		}

		function getCurrentLanguage() {
		    return $translate.use();
		}

	  function changeLanguage(newLang){
	      $translate.use(newLang);
	  }
	};
})();
angular.module('pascalprecht.translate')
.factory('$translateStaticFilesLoader', ['$q', '$http', function (a, b) {
    return function (c) {
        if (!c || !angular.isString(c.prefix) || !angular.isString(c.suffix))
            throw new Error("Couldn't load static files, no prefix or suffix specified!");
        var d = a.defer();
        return b({ url: [c.prefix, c.key, c.suffix].join(""), method: "GET", params: "" })
            .success(function (a) { d.resolve(a) })
            .error(function () { d.reject(c.key) }), d.promise
    }
}]);
