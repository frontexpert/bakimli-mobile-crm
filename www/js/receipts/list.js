(function () {
  'use strict';

	var controllerId = 'receiptslist';
	angular.module('bakimliApp.receipts')
		.controller(controllerId,
			['$rootScope', '$state', '$ionicPopup', 'receiptsServices', 'Utils', '$log', '$ionicScrollDelegate', receiptslist]);

	function receiptslist($rootScope, $state, $ionicPopup, receiptsServices, Utils, $log, $ionicScrollDelegate) {

		var vm = this;

		// init controller
		vm.init = function() {
			vm.receipts = {};
			// show logout button in this view
			$rootScope.hideLogout = true;
			vm.isMoreReceipts = true;
			vm.options = {
	      page: 0,
				page_size: 10
	    };
		}

		vm.add = function() {
			$state.go('app.cart');
		};

		vm.fetchReceipts = function(options) {
	    receiptsServices.getReceipts(vm.options).then(function(data) {
	      if (vm.options.page === 0) {
	        $ionicScrollDelegate.scrollTop();
	      }
				vm.receipts = vm.groupByDate(data.results);
	      vm.isMoreReceipts = data.next;
	      $rootScope.$broadcast('scroll.infiniteScrollComplete');
				$rootScope.$broadcast('scroll.refreshComplete');
	    }, function () {
	      vm.options.page--;
	      $rootScope.$broadcast('scroll.infiniteScrollComplete');
				$rootScope.$broadcast('scroll.refreshComplete');
	    });
	  };

		vm.fetchMoreReceipts = function() {
			if(Utils.getCurrentStateName() === 'app.receipts'){
	      vm.options.page++;
				vm.fetchReceipts(vm.options);
	    }else{
	      $log.debug('should not load data here');
	      $rootScope.$broadcast('scroll.infiniteScrollComplete');
	    }
		};

		vm.groupByDate = function groupByDate(array){
	      var grouped = vm.receipts;
	      _.forEach(array, function(value){
					var actualDay = new Date(value.order_date).toJSON().slice(0,10);

	          // var actualDay = new Date(value.order_date).getDate();
	          if ( !grouped[actualDay] ){
	            grouped[actualDay] = [];
	          }
	          grouped[actualDay].push(value);
	      });

				// calculate total price by day
				for (var prop in grouped) {
					var total = 0;
				  console.log("obj." + prop + " = " + grouped[prop]);
					for (var i = 0; i < grouped[prop].length; i++) {
						total += parseFloat(grouped[prop][i].total);
					}
					grouped[prop].totalDay = total;
				}

	      return grouped;
	  };

		vm.init();
	}
})();
