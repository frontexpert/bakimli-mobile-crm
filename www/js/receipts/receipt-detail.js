(function () {
  'use strict';

	var controllerId = 'receiptDetail';
	angular.module('bakimliApp.receipts')
		.controller(controllerId,
			['$rootScope', '$stateParams', 'receiptsServices', receiptDetail]);

	function receiptDetail($scope, $stateParams, receiptsServices) {

		var vm = this;
		var pk = $stateParams.pk;

		vm.receipt = {};

		// initialize this view
		vm.init = function() {
			// fetch receipt by pk
			receiptsServices.getReceiptByPk(pk).then(function(success) {
				console.log("Fetched selected receipt");
				vm.receipt = success;
			}, function(error) {
				console.log("Error :", error);
			});
		};

		vm.init();
	}

})();
