(function () {
  'use strict';

	var controllerId = 'reviewCart';
	angular.module('bakimliApp.receipts')
		.controller(controllerId,
			['$rootScope', '$state', '$ionicHistory', 'cartProperties', 'receiptsServices', 'UIHelper', 'Utils', reviewCart]);

	function reviewCart($rootScope, $state,	$ionicHistory, cartProperties, receiptsServices, UIHelper, Utils) {

		var vm = this;

		$rootScope.hideLogout = true;

		vm.doSubmitCart = function() {
			// calculate cart
			var receiptCart = _calcuateCart(vm.cart);

			if (angular.isDefined(receiptCart)) {
				receiptsServices.createReceipt(receiptCart)
				.then(function(success) {
					console.log("Successful craeted :", success);
					// reset cart
					cartProperties.reset();
					$ionicHistory.nextViewOptions({
					  disableAnimate: false,
					  disableBack: true
					});
					// goto list
					$state.go('app.receipts');
				}, function(error) {
					UIHelper.showAlert(error);
				});
			}
		};

		vm.init = function() {
			vm.cart = cartProperties;
		};

		vm.init();


		var _calcuateCart = function(cart) {
			// format receipt data for post server
			var receipt = {
				order_date: cart.appointmentTime,
				client: "",
				subtotal: "", 			// required
				discount_total: 0, 	// required
				total: "", 					// required
				cash: 0,
				credit_cart: 0,
				notes: "",
				salon: null,
				services: [], 			// required
				products: [], 			// required
				professional: "" 		// required
			};

			if ( !Utils.isUndefinedOrNull(cart.client)) {
				receipt.client = cart.client.pk;
			}
			else {
				UIHelper.showAlert("You must select a client.");
				return undefined;
			}

			receipt.subtotal = receipt.total = cart.total;

			// services template
			var serviceTemplate = {
				service: "",
				professional: "",
				quantity: 1, // default
				unit_price: 0,
				discount_percentage: 0, // default
				subtotal: 0
			};
			for (var i = cart.services.length - 1; i >= 0; i--) {
				var service = serviceTemplate;
				// set service pk
				if (angular.isDefined(cart.services[i].service)) {
					service.service = cart.services[i].service.pk;
				}
				// set professional pk
				if (angular.isDefined(cart.services[i].professional)) {
					service.professional = cart.services[i].professional.pk;
				}

				// set unit_price
				service.unit_price = cart.services[i].price;

				// set quantity if had quantity else ues default 0
				if (angular.isDefined(cart.services[i].quantity)) {
					service.quantity = cart.services[i].quantity;
				}

				// set discount percentage if had one else use default 0
				if (angular.isDefined(cart.services[i].discount_percentage)) {
					service.discount_percentage = cart.services[i].discount_percentage;
				}

				// calc subtotal
				service.subtotal = (service.unit_price * service.quantity) * (1 - service.discount_percentage);

				// push to receipt services
				receipt.services.push(angular.copy(service));
			}

			// services template
			var productTemplate = {
				product: "",
				quantity: 1, // default
				unit_price: 0,
				discount_percentage: 0, // default
				subtotal: 0
			};
			for (var i = cart.products.length - 1; i >= 0; i--) {
				var product = productTemplate;

				// set service pk
				if (angular.isDefined(cart.products[i].product)) {
					product.product = cart.products[i].product.pk;
				}
				// set unit_price
				product.unit_price = cart.products[i].price;

				// set quantity if had quantity else ues default 0
				if (angular.isDefined(cart.products[i].quantity)) {
					product.quantity = cart.products[i].quantity;
				}

				// set discount percentage if had one else use default 0
				if (angular.isDefined(cart.products[i].discount_percentage)) {
					product.discount_percentage = cart.products[i].discount_percentage;
				}

				// calc subtotal
				product.subtotal = (product.unit_price * product.quantity) * (1 - product.discount_percentage);

				receipt.products.push(angular.copy(product));
			}

			// set professional pk
			receipt.professional = cart.proId;

			return receipt;
		};
	}
})();
