(function () {
  'use strict';

  var app = angular.module('bakimliApp.receipts', []);

  app.config([
    '$stateProvider',
    '$urlRouterProvider',
    function(
    $stateProvider,
    $urlRouterProvider
  ){

    $stateProvider
    .state('app.receipts', {
    	cache: false,
  		url: '/receipts',
  		views: {
  			'menuContent': {
  				templateUrl: 'js/receipts/list.html',
  				controller: 'receiptslist as vm'
  			}
  		}
  	})
  	// receipt detail
  	.state('app.receipt', {
  		url: '/receipt/:pk',
  		views: {
  			'menuContent': {
  				templateUrl: 'js/receipts/receipt-detail.html',
  				controller: 'receiptDetail as vm'
  			}
  		}
  	})
  	// create a receipt
  	.state('app.cart', {
  		cache: false,
  		url: '/receipts/cart/:clientId',
  		views: {
  			'menuContent': {
  				templateUrl: 'js/receipts/cart.html',
  				controller: 'receitpCart as vm'
  			}
  		}
  	})
  	.state('app.selclient', {
  		url: '/receipts/selclient',
  		views: {
  			'menuContent': {
  				templateUrl: 'js/receipts/select-client.html',
  				controller: 'selectClient as vm'
  			}
  		}
  	})
    .state('app.create-client', {
  		url: '/receipts/create-client/:proId',
  		views: {
  			'menuContent': {
  				templateUrl: 'js/receipts/create-client.html',
  				controller: 'createClient as vm'
  			}
  		}
  	})
  	.state('app.reveiwcart', {
  		cache: false,
  		url: '/receipts/reveiwcart',
  		views: {
  			'menuContent': {
  				templateUrl: 'js/receipts/review-cart.html',
  				controller: 'reviewCart as vm'
  			}
  		}
  	});
  }]);
})();
