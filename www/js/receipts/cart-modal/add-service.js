(function () {
  'use strict';

  var controllerId = 'addSeviceModal';
  angular.module('bakimliApp.receipts')
    .controller(controllerId,
      ['$scope', 'parameters', 'cartProperties', 'professionalsService', addSeviceModal]);

  function addSeviceModal($scope, parameters, cartProperties, professionalsService){
    var vm = this;

    if (angular.isDefined(parameters)) {
      vm.selectedService = parameters;
    }
    else {
      vm.selectedService = {};
    }
    vm.services = {};
    vm.editStatus = false;
    vm.invalid = {
      client: false,
      price: false
    };

    vm.init = function() {
      if (Object.keys(vm.selectedService).length > 0) {
        // only can edit price
        vm.editStatus = true;
      }
      else {
        professionalsService.getServicesByPk(cartProperties.proId)
        .then(function(success) {
          if (success.count > 0)
            vm.services = success.results;
        }, function(error) {
          console.log("Error :", error);
        });
      }
    };
    // initialize modal
    vm.init();

    vm.confirm = function(value){
      if ( angular.isDefined(value.service) ) {
        if (value.price !== null && value.price !== "") {
          $scope.closeModal(value);
        }
        else {
          vm.invalid.price = true;
        }
      }
      else {
        vm.invalid.client = true;
        vm.invalid.price = true;
      }
    };
    vm.cancel = function(){
      $scope.closeModal(null);
    };
    vm.update = function() {
      vm.invalid.client = false;
    };
    vm.updatePrice = function(value) {
      vm.invalid.price = false;
    };
  }
})();
