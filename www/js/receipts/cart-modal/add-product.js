(function () {
  'use strict';

  var controllerId = 'addProductModal';
  angular.module('bakimliApp.receipts')
    .controller(controllerId,
      ['$scope', 'parameters', 'cartProperties', 'professionalsService', addProductModal]);

  function addProductModal($scope, parameters, cartProperties, professionalsService){
    var vm = this;
    if (angular.isDefined(parameters)) {
      vm.selectedProduct = parameters;
    }
    else {
      vm.selectedProduct = {};
    }
    vm.products = {};
    vm.editStatus = false;
    vm.invalid = {
      product: false,
      price: false
    };

    vm.init = function() {
      if (Object.keys(vm.selectedProduct).length > 0) {
        // only can edit price
        vm.editStatus = true;
      }
      else {
        professionalsService.getProductsByPk(cartProperties.proId)
        .then(function(success) {
          if (success.count > 0)
            vm.products = success.results;
        }, function(error) {
          console.log("Error :", error);
        });
      }
    };

    // initialize modal
    vm.init();

    vm.confirm = function(value){
      if ( angular.isDefined(value.product) ) {
        if (value.price !== null && value.price !== "") {
          $scope.closeModal(value);
        }
        else {
          vm.invalid.price = true;
        }
      }
      else {
        vm.invalid.product = true;
        vm.invalid.price = true;
      }
    };
    vm.cancel = function(){
      $scope.closeModal(null);
    };
    vm.update = function() {
      vm.invalid.product = false;
    };
    vm.updatePrice = function(value) {
      vm.invalid.price = false;
    };
  }
})();
