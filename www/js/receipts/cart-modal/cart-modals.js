(function () {
  'use strict';

  angular.module('bakimliApp.receipts').factory('cartModals',
    ['bakimliModal', cartModals]);

  function cartModals(bakimliModal){
  // all cart modals here
    var service = {
      showAddEditProduct: showAddEditProduct,
      showAddEditService: showAddEditService
    };

    return service;

    function showAddEditProduct(param){
      return bakimliModal.show('js/receipts/cart-modal/add-product.html', 'addProductModal as vm', param);
    }

    function showAddEditService(param) {
      return bakimliModal.show('js/receipts/cart-modal/add-service.html', 'addSeviceModal as vm', param);
    }
  }
})();
