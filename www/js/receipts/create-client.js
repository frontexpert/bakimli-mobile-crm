(function () {
  'use strict';

	var controllerId = 'createClient';
  angular.module('bakimliApp.receipts')
    .controller(controllerId, ['$state', '$stateParams', 'professionalsService', 'cartProperties', 'UIHelper', '$ionicHistory', '$log', createClient]);

  function createClient($state, $stateParams, professionalsService, cartProperties, UIHelper, $ionicHistory, $log) {

    var vm = this;
    if(!$stateParams.proId){
      $state.go('app.receipts');
      return;
    };

    // initialize view
    vm.init = function() {
      vm.genderOptions = [
        { name: 'Unknow', value: 'u' },
        { name: 'Female', value: 'f' },
        { name: 'Male', value: 'm' }
      ];
      vm.client = {};
      vm.client.gender = 'u';
      vm.proId = $stateParams.proId;
    };

    vm.doCreate = function() {
      if (vm.proId && vm.client) {
        professionalsService.createProfessionalClient(vm.proId, vm.client)
        .then(function(success){
          $log.debug('Successfully create a client.');
          cartProperties.client = success;
          $ionicHistory.goBack(-2);
        }, function(error){
          if (error.detail) {
            UIHelper.showAlert(error.detail);
          }
          $log.debug('Faild create a client.');
        });
      } else {
        UIHelper.showAlert('Something went wrong.');
      }
    };

    vm.init();
  }
})();
