(function () {
  'use strict';

  var controllerId = 'receitpCart';
  angular.module('bakimliApp.receipts')
    .controller(controllerId,
      ['$rootScope', '$state', '$stateParams', 'receiptsServices', 'clientsService', 'cartProperties', 'cartModals', 'sessionService', receitpCart]);

  function receitpCart($rootScope, $state, $stateParams, receiptsServices, clientsService, cartProperties, cartModals, sessionService) {

    var vm = this;

    var clientId = $stateParams.clientId;

  	vm.editServicePrice = function(item) {
      console.log('Edit Item: ' , item.price);

      // Preset cart.service values
      cartModals.showAddEditService(item).then(
      function(result) {
        if(result){
          // price will be update automatically.
          vm.cart.updateTotal();
        }
      }, function(err) {
        alert(err);
      });
    };

    // Remove selected service in the products
    vm.removeService = function(item) {
      console.log('Remove Item: ' + item.name);
      // Search & Destroy item from list
      vm.cart.services.splice(vm.cart.services.indexOf(item), 1);
      vm.cart.updateTotal();
    };

    vm.editProductPrice = function(item) {
      console.log('Edit Item: ', item.price);
      // Preset cart.product values
      cartModals.showAddEditProduct(item).then(
      function(result) {
        if(result){
          // price will be update automatically.
          vm.cart.updateTotal();
        }
      }, function(err) {
        alert(err);
      });
    };

    // Remove selected product in the products
    vm.removeProduct = function(item) {
      console.log('Remove Item: ', item.name);
      vm.cart.products.splice(vm.cart.products.indexOf(item), 1);
      vm.cart.updateTotal();
    };

    // goto review page
    vm.doReviewCart = function() {
      // cartProperties.setCart(vm.cart);
      $state.go('app.reveiwcart');
    };

    // Add service
    vm.addService = function() {
      cartModals.showAddEditService().then(
      function(result) {
        if(result){
          vm.cart.services.push(angular.copy(result));
          vm.cart.updateTotal();
        }
      }, function(err) {
        alert(err);
      });
      // vm.updateTotal();
    };

    // Add product
    vm.addProduct = function() {
      cartModals.showAddEditProduct().then(
        function(result) {
          if (result) {
            vm.cart.products.push(angular.copy(result));
            vm.cart.updateTotal();
          }
        }, function(err) {
          alert(err);
        }
      );
      // vm.updateTotal();
    };

    // initialize view
    vm.init = function() {
      if ( angular.isDefined(clientId) && clientId !== "" ) {
        // get clients
        clientsService.getClientByPk(clientId)
        .then(function(success) {
          vm.cart.client = success;
        }, function(error) {
          console.log("Error: ", error);
        })
      }
      vm.cart = cartProperties;
      // set professional pk by logged in user
      vm.cart.proId = sessionService.getUserInfo().professional.pk;
      $rootScope.hideLogout = true;
    };

    vm.init();
  }
})();
