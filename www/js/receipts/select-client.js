(function () {
  'use strict';

	var controllerId = 'selectClient';
	angular.module('bakimliApp.receipts')
		.controller(controllerId, ['$state', 'professionalsService', 'Utils',	'cartProperties',	selectClient]);

	function selectClient($state,	professionalsService,	Utils, cartProperties) {

		var vm = this;

		vm.clients = {};

		// initialize view
		vm.init = function() {
			if (cartProperties.proId) {
				// get clients
				professionalsService.getProfessionalClientsByPk(cartProperties.proId).then(
				function(success) {
					vm.clients = success;
				},
				function(error) {
					// error
					console.log("Error:", error);
				});
			} else {
				vm.message = "You don't have clients.";
			}
		};

		vm.init();

		vm.doSelectClient = function(selectedClient) {
			// set cart properties
			cartProperties.client = selectedClient;

			Utils.goBack();
		};

		// Craete a client
		vm.create = function() {
			$state.go('app.create-client', {proId: cartProperties.proId});
		}
	}
})();
